## Belka & Strelka
-
This is a simple node.js application to generate UUIDs of Version 4(random) (i.e created from cryptographically-strong random values) 


### Installation

 *On your machine locally:* 

To make this app work locally on your machine just run
`yarn start ` from root project directory.

Note!

In case you want to load this app in **development** mode start this application with `yarn dev`. "Nodemon" library will reload the application every time a change in code appears.

 
 *In a docker container:*
 
 Make note of these files: `build.sh`, `run.sh` and `stop.sh`. 

 Take the following steps: 
  1. Execute `build.sh` (the script will build an image)
  2. Execute `run.sh` (to start a container based on the image built in the previous step)
  3. Once you’re satisfied that your container works correctly, you can delete it with `stop.sh`.
  

  ### How to use?
  
  Regardless of what installation method you chose, you can check the app on your local port 8080.
  Type in the address line of any browser on your system the following: `http://localhost:8080/id`.

  ### Modification
  
