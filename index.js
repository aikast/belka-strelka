const express = require('express');

const controllers = require('./controllers');


const app = express();
const port = 8080;

app.use(express.json());

app.use('/', controllers);

app.listen(port, () => {
    console.log(`Server is running on port ${port}!`)
});

module.exports = app;
