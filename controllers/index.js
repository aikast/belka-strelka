const express = require('express');

const router = express.Router();

router.use('/id', require('./id'));

router.use('/hello', async(req, res) => {
   try {
       res.status(200).send({"message": "Hello world!"})
   }  catch (error) {
       res.status(400).send({error})
   }
});

module.exports = router;




