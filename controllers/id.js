const express = require('express');
const generateId = require('./generator');

const router = express.Router();

const myId = generateId();

router.use('/', async (req, res) => {

    const myObj = {
        prop1: myId,
        prop2: "hello world"
    };
    res.status(200).send(myObj);
});

module.exports = router;
