const { v4: uuidV4 } = require('uuid');

const generateId = () => (uuidV4());

module.exports = generateId;
